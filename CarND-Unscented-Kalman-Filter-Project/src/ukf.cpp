#include "ukf.h"
#include "Eigen/Dense"
#include <iostream>

using namespace std;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;

/**
 * Initializes Unscented Kalman filter
 * This is scaffolding, do not modify
 */
UKF::UKF() 
{
  is_initialized_ = false;
  
  // if this is false, laser measurements will be ignored (except during init)
  use_laser_ = true;

  // if this is false, radar measurements will be ignored (except during init)
  use_radar_ = true;

  // time when the state is true, in us
  time_us_ = 0.0;

  // State dimension
  n_x_ = 5;

  // initial state vector
  x_ = VectorXd(n_x_); //5x1

  // initial covariance matrix
  P_ = MatrixXd(x_.size(), x_.size()); // 5x5

  // Process noise standard deviation longitudinal acceleration in m/s^2
  std_a_ = 2; 

  // Process noise standard deviation yaw acceleration in rad/s^2
  std_yawdd_ = 2;
  
  //DO NOT MODIFY measurement noise values below these are provided by the sensor manufacturer.
  // Laser measurement noise standard deviation position1 in m
  std_laspx_ = 0.15;

  // Laser measurement noise standard deviation position2 in m
  std_laspy_ = 0.15;

  // Radar measurement noise standard deviation radius in m
  std_radr_ = 0.3;

  // Radar measurement noise standard deviation angle in rad
  std_radphi_ = 0.03;

  // Radar measurement noise standard deviation radius change in m/s
  std_radrd_ = 0.3;
  //DO NOT MODIFY measurement noise values above these are provided by the sensor manufacturer.
  
  // Augmented state dimension
  n_aug_ = n_x_ + 2;

  // initial weights
  weights_ = VectorXd(2 * n_aug_ + 1); // 15x1

  // Augmented sigma points matrix
  Xsig_aug_ = MatrixXd(n_aug_, 2 * n_aug_ + 1); // 7x15

  // predicted sigma points matrix
  Xsig_pred_ = MatrixXd(n_x_, 2 * n_aug_ + 1); // 5x15 

  // Sigma point spreading parameter
  lambda_ = 3 - n_aug_;
}

UKF::~UKF() {}

/*
 * @param {MeasurementPackage} meas_package The latest measurement data of
 * either radar or laser.
 * Pipeline steps:
 1) Initialize
 2) Generate augmented sigma points
 3) Prediction step (apply process model)
    a) Predict sigma points in state space 
	b) Calculate predicted mean in state space
	c) Calculate predicted covariance matrix in state space
 4) Update step (sensor dependent)
	a) Transfrom predicted sigma points from state space 
	   to measurement space (apply measurement model)
	b) Calculate predicted mean in measurement space
	c) Calculate predicted covariance matrix in measurement space
	d) Update state
	e) Update covariance matrix
 */
void UKF::ProcessMeasurement(const MeasurementPackage& meas_package) 
{
	if (!is_initialized_)
	{
        // state vector 
		x_.fill(0.0); // 5x1

		// intialize state covariance matrix
		P_.fill(0.0); // 5x5
		P_(0, 0) = 0.0225;
		P_(1, 1) = 0.0225;
		P_(2, 2) = 0.0225;
		P_(3, 3) = 0.0225;
		P_(4, 4) = 0.0225;

		// Augmented sigma points matrix
		Xsig_aug_.fill(0.0); // 5x15

		// Predicted sigma points matrix
		Xsig_pred_.fill(0.0); // 7x15

		// Weights of sigma points
		weights_(0) = lambda_ / (lambda_ + n_aug_);
		for (int i = 1; i < 2*n_aug_+1; ++i)
		{
			weights_(i) = 0.5 / (lambda_ + n_aug_);
		}

		if (meas_package.sensor_type_ == MeasurementPackage::RADAR)
		{
			/**
			Convert radar from polar to cartesian coordinates and initialize state.
			*/
			VectorXd z = meas_package.raw_measurements_;
			double px = z[0] * cos(z[1]);
			double py = z[0] * sin(z[1]);
			x_ << px, py, 5.0, 0.0, 0.0;
		}
		else if (meas_package.sensor_type_ == MeasurementPackage::LASER)
		{
			/**
			Initialize state.
			*/
			VectorXd z = meas_package.raw_measurements_;
			x_ << z[0], z[1], 5.0, 0.0, 0.0;
		}

		time_us_ = meas_package.timestamp_;

		// done initializing, no need to predict or update
		is_initialized_ = true;

		return;
	}
	double dt = (meas_package.timestamp_ - time_us_) / 1000000.0;
	time_us_ = meas_package.timestamp_;

	// Generate augmented sigma points
	GenerateAugmentedSigmaPoints();

	// Predict
	Predict(dt);

	// Update
	if (meas_package.sensor_type_ == MeasurementPackage::RADAR)
	{
		if (use_radar_)
		{
			// Radar updates
			UpdateRadar(meas_package);
		}
	}
	else if (meas_package.sensor_type_ == MeasurementPackage::LASER)
	{
		if (use_laser_)
		{		
			// Laser updates
			UpdateLidar(meas_package);
		}
	}

	// print the output
	cout << "x_ = " << x_ << endl;
	cout << "P_ = " << P_ << endl;

	return;
}

void UKF::GenerateAugmentedSigmaPoints()
{
	//create augmented mean vector
	VectorXd x_aug = VectorXd(n_aug_); // 7x1

	//create augmented state covariance
	MatrixXd P_aug = MatrixXd(n_aug_, n_aug_); // 7x7

	//create augmented mean state
	x_aug.head(n_x_) = x_;
	x_aug(n_x_) = 0;
	x_aug(n_x_ + 1) = 0;

	//create augmented covariance matrix
	P_aug.fill(0.0);
	P_aug.topLeftCorner(n_x_, n_x_) = P_;
	P_aug(n_x_, n_x_) = std_a_ * std_a_;
	P_aug(n_x_ + 1, n_x_ + 1) = std_yawdd_ * std_yawdd_;

	//create square root matrix
	MatrixXd L = P_aug.llt().matrixL(); // 7x7

	//create augmented sigma points
	Xsig_aug_.col(0) = x_aug;
	for (int i = 0; i < n_aug_; i++)
	{
		Xsig_aug_.col(i + 1) = x_aug + sqrt(lambda_ + n_aug_) * L.col(i);
		Xsig_aug_.col(i + 1 + n_aug_) = x_aug - sqrt(lambda_ + n_aug_) * L.col(i);
	}

	return;
}

/**
 * Predicts sigma points, the state, and the state covariance matrix.
 * Prediction step (apply process model)
	a) Predict sigma points in state space
	b) Calculate predicted mean in state space
	c) Calculate predicted covariance matrix in state space 
 * @param {double} delta_t the change in time (in seconds) between the last
 * measurement and this one.
 */
void UKF::Predict(const double& dt) 
{
	// predict sigma points by feeding augmented sigma points through the process model
	// Xsig_aug_ (size 7 x 15) -> process model = Xsig_pred_ (size 5 x 15)
	for (int i = 0; i < 2*n_aug_+1; ++i)
	{
		VectorXd sig_point = Xsig_aug_.col(i);
		double v = sig_point(2);

		double yaw_angle = sig_point(3);
		//angle normalization
		while (yaw_angle > M_PI) { yaw_angle -= 2.*M_PI; }
		while (yaw_angle < M_PI) { yaw_angle += 2.*M_PI; }

		double yaw_rate = sig_point(4);
		double nu_a = sig_point(5);
		double nu_yaw = sig_point(6);

		VectorXd pred(n_x_);
		pred.fill(0.0);
		if (fabs(yaw_rate) > 0.001) 
		{
			pred(0) = sig_point(0) + (v/yaw_rate)*( sin(yaw_angle + (yaw_rate*dt)) - sin(yaw_angle)) + (0.5*dt*dt*cos(yaw_angle)*nu_a);
			pred(1) = sig_point(1) + (v/yaw_rate)*(-cos(yaw_angle + (yaw_rate*dt)) + cos(yaw_angle)) + (0.5*dt*dt*sin(yaw_angle)*nu_a);
		}
		else
		{
			pred(0) = sig_point(0) + (v*cos(yaw_angle)*dt) + (0.5*dt*dt*cos(yaw_angle)*nu_a);
			pred(1) = sig_point(1) + (v*sin(yaw_angle)*dt) + (0.5*dt*dt*sin(yaw_angle)*nu_a);
		}

		pred(2) = sig_point(2) + (dt*nu_a);
		pred(3) = sig_point(3) + (yaw_rate*dt) + (0.5*dt*dt*nu_yaw);
		pred(4) = sig_point(4) + (dt*nu_yaw);

		Xsig_pred_.col(i) = pred;
	}

	// predict state mean from predicted sigma points
	x_.fill(0.0); // maybe initializing with zeros is needed
	x_ += (Xsig_pred_*weights_);  // 5x1 + 5x15 * 15x1 = 5x1

	//predict state covariance matrix from predicted sigma points and predicted state mean
	P_.fill(0.0); // maybe initializing with zeros is needed
	for (int j = 0; j < 2*n_aug_+1; ++j) // maybe initializing with zeros is needed
	{  //iterate over sigma points

	   // state difference
		VectorXd x_diff = Xsig_pred_.col(j) - x_; // 5x1
		//angle normalization
		while (x_diff(3) > M_PI)  { x_diff(3) -= 2.*M_PI; }
		while (x_diff(3) < -M_PI) { x_diff(3) += 2.*M_PI; }

		P_ += (weights_(j)*x_diff*(x_diff.transpose())); //5x5 + 5x1*1x5 = 5x5
	}
}

/**
 * Predict radar measurement by transforming predicted sigma points to radar measurement space.
	 a) Calculate predicted mean in measurement space
	 b) Calculate predicted covariance matrix in measurement space
 * Updates the state and the state covariance matrix using a radar measurement.
 * @param {MeasurementPackage} meas_package
 */
void UKF::UpdateRadar(const MeasurementPackage& meas_package) 
{
	/* Predict radar measurement by transforming predicted sigma points to radar measurement space. */
	// number of radar dimensions
	const int n_z = 3;

	// create matrix for sigma points in measurement space
	MatrixXd Zsig = MatrixXd(n_z, 2 * n_aug_ + 1); // 3 x 15

	// mean predicted measurement
	VectorXd z_pred = VectorXd(n_z); 

	// measurement covariance matrix S
	MatrixXd S = MatrixXd(n_z, n_z); // 3x3

	//transform sigma points into measurement space
	for (int i = 0; i < 2*n_aug_+1; ++i)
	{
		double px = Xsig_pred_(0, i);
		double py = Xsig_pred_(1, i);
		double v =  Xsig_pred_(2, i);
		double yaw_angle = Xsig_pred_(3, i);
		while (yaw_angle < -M_PI) { yaw_angle += 2 * M_PI; }
		while (yaw_angle > M_PI)  { yaw_angle -= 2 * M_PI; }

		double v1 = cos(yaw_angle)*v;
		double v2 = sin(yaw_angle)*v;
	    
		double rho = sqrt(px*px + py*py);
		double phi = atan2(py, px);
		while (phi < -M_PI) { phi += 2 * M_PI; }
		while (phi > M_PI)  { phi -= 2 * M_PI; }
		
		double range;		
		if (fabs(rho) > 0.001)
		{
			range = (px*cos(yaw_angle)*v + py*sin(yaw_angle)*v) / rho;
		}
		else
		{
			rho = 0.0;
			range = 0.0;
		}		

		Zsig.col(i) << rho, phi, range;		
	}
	// calculate mean predicted measurement
	z_pred.fill(0.0);
	z_pred += (Zsig * weights_); // 3x15 * 15x1 = 3x1

	// calculate measurement noise matrix R
	MatrixXd R(n_z, n_z); // 3x3
	R.fill(0.0);
	R(0, 0) = std_radr_ * std_radr_;
	R(1, 1) = std_radphi_ * std_radphi_;
	R(2, 2) = std_radrd_ * std_radrd_;

	// calculate measurement covariance matrix S
	S.fill(0.0); // 3x3
	for (int j = 0; j < 2 * n_aug_ + 1; j++)
	{  //iterate over sigma points

	    // difference between sigma points and predicted mean in measurement space
		VectorXd z_diff_temp1 = Zsig.col(j) - z_pred;
		//angle normalization
		while (z_diff_temp1(1) > M_PI)  { z_diff_temp1(1) -= 2.*M_PI; }
		while (z_diff_temp1(1) < -M_PI) { z_diff_temp1(1) += 2.*M_PI; }

		S += (weights_(j) * z_diff_temp1 * (z_diff_temp1.transpose())); // 3x3 + 3x1 * 1x3 = 3x3
	}

	S += R;	// 3x3 + 3x3 = 3x3
	
	/* Update state and covariance matrix */

	// get the true measurement
	VectorXd z = meas_package.raw_measurements_;
	
	// create matrix for cross correlation Tc
	MatrixXd Tc = MatrixXd(n_x_, n_z); // 5x3

	// calculate cross correlation matrix
	Tc.fill(0.0);
	for (int k = 0; k < 2*n_aug_ + 1; ++k) {  //2n+1 sigma points

		// difference between sigma points and predicted mean in measurement space
		VectorXd z_diff_temp2 = Zsig.col(k) - z_pred; // 3x1
		//angle normalization
		while (z_diff_temp2(1) > M_PI)  { z_diff_temp2(1) -= 2.*M_PI; }
		while (z_diff_temp2(1) < -M_PI) { z_diff_temp2(1) += 2.*M_PI; }

		// difference between sigma points and predicted mean in state space
		VectorXd x_diff = Xsig_pred_.col(k) - x_; // 5x1
		//angle normalization
		while (x_diff(3) > M_PI)  { x_diff(3) -= 2.*M_PI; }
		while (x_diff(3) < -M_PI) { x_diff(3) += 2.*M_PI; }

		Tc += (weights_(k) * x_diff * z_diff_temp2.transpose()); // 5x3 + 5x1 * 1x3 = 5x3
	}

	//Kalman gain K;
	MatrixXd K = Tc * (S.inverse()); // 5x3 x 3x3 = 5x3

	//residual with respect to measurement
	VectorXd z_diff = z - z_pred; // 3x1

	//angle normalization
	while (z_diff(1) > M_PI)  { z_diff(1) -= 2.*M_PI; }
	while (z_diff(1) < -M_PI) { z_diff(1) += 2.*M_PI; }

	//update state mean and covariance matrix
	x_ += K * z_diff; // 5x3 x 3x1 = 5x1
	P_ -= K * S * (K.transpose()); // 5x5 - 5x3 x 3x3 x 3x5 = 5x5

	// Calculate radar NIS
	nis_radar_ = (z_diff.transpose()) * (S.inverse()) * z_diff;
}

/**
* Predict lidar measurement by transforming predicted sigma points to lidar measurement space.
	a) Calculate predicted mean in measurement space
	b) Calculate predicted covariance matrix in measurement space
* Updates the state and the state covariance matrix using a radar measurement.
* @param {MeasurementPackage} meas_package
*/
void UKF::UpdateLidar(const MeasurementPackage& meas_package)
{
	/* Predict lidar measurement by transforming predicted sigma points to radar measurement space. */
	// number of lidar dimensions
	const int n_z = 2;

	// create matrix for sigma points in measurement space
	MatrixXd Zsig = MatrixXd(n_z, 2*n_aug_+1); // 2 x 15

	// mean predicted measurement
	VectorXd z_pred = VectorXd(n_z);

	// measurement covariance matrix S
	MatrixXd S = MatrixXd(n_z, n_z); // 2x2

	//transform predicted sigma points into measurement space
	for (int i = 0; i < 2*n_aug_+1; ++i)
	{
		double px = Xsig_pred_(0, i);
		double py = Xsig_pred_(1, i);

		Zsig.col(i) << px, py;
	}
	//calculate mean predicted measurement
	z_pred.fill(0.0); // 2x1
	z_pred += (Zsig * weights_); // 2x15 * 15x1 = 2x1

	//calculate measurement noise matrix R
	MatrixXd R(n_z, n_z); //2x2
	R.fill(0.0);
	R(0, 0) = std_laspx_ * std_laspx_;
	R(1, 1) = std_laspy_ * std_laspy_;

	// calculate measurement covariance matrix S
	S.fill(0.0); // 2x2
	for (int j = 0; j < 2 * n_aug_ + 1; j++)
	{  //iterate over sigma points

	   // difference between sigma points and predicted mean in measurement space
		VectorXd z_diff_temp1 = Zsig.col(j) - z_pred;

		S += (weights_(j) * z_diff_temp1 * (z_diff_temp1.transpose())); // 2x2 + 2x1 * 1x2 = 2x2
	}

	S += R;	// 2x2 + 2x2 = 2x2

	/* Update state and covariance matrix */

	// get the true measurement
	VectorXd z = meas_package.raw_measurements_;

	// create matrix for cross correlation Tc
	MatrixXd Tc = MatrixXd(n_x_, n_z); // 5x2

	// calculate cross correlation matrix
	Tc.fill(0.0);
	for (int k = 0; k < 2*n_aug_+1; ++k) {  //2n+1 sigma points

		// difference between sigma points and predicted mean in measurement space
		VectorXd z_diff_temp2 = Zsig.col(k) - z_pred; // 2x1

		// difference between sigma points and predicted mean in state space
		VectorXd x_diff = Xsig_pred_.col(k) - x_; // 5x1
		//angle normalization
		while (x_diff(3) > M_PI)  { x_diff(3) -= 2.*M_PI; }
		while (x_diff(3) < -M_PI) { x_diff(3) += 2.*M_PI; }

		Tc += (weights_(k) * x_diff * (z_diff_temp2.transpose())); // 5x2 + 5x1 * 1x2 = 5x2
	}

	// Kalman gain K;
	MatrixXd K = Tc * S.inverse(); // 5x2 x 2x2 = 5x2

	// residual with respect to measurement
	VectorXd z_diff = z - z_pred; // 2x1

	// update state mean and covariance matrix
	x_ += (K * z_diff); // 5x2 x 2x1 = 5x1
	P_ -= (K * S * (K.transpose())); // 5x5 - 5x2 x 2x2 x 2x5 = 5x5

	// Calculate lidar NIS
	nis_lidar_ = (z_diff.transpose()) * (S.inverse()) * z_diff;
}

