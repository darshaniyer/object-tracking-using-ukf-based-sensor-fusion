#include <iostream>
#include "tools.h"

using Eigen::VectorXd;
using Eigen::MatrixXd;
using std::vector;

Tools::Tools() {}

Tools::~Tools() {}

VectorXd Tools::CalculateRMSE(const vector<VectorXd>& estimations,
							  const vector<VectorXd>& ground_truth)
{
	VectorXd rmse(6);
	rmse << 1, 2, 3, 4, 5, 6;

	if ((estimations.size() > 0) && (estimations.size() == ground_truth.size()))
	{
		rmse << 0, 0, 0, 0, 0, 0;
		for (int i = 0; i < ground_truth.size(); ++i)
		{
			VectorXd residual = ground_truth[i] - estimations[i]; // 6x1 - 6x1 = 6x1
			residual = residual.array()*residual.array();

			rmse += residual; // 6x1 + 6x1 = 6x1
		}

		rmse /= ground_truth.size();

		rmse = rmse.array().sqrt();
	}
	else
	{
		cout << "Invalid estimation or ground truth data" << endl;
	}

	return rmse;
}