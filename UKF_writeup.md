
## Bicycle tracking using the sensor fusion algorithm based on unscented Kalman filter

A self-driving car expert takes input from all the data sources and turns them into coherent picture. For example, a lidar is able to see distances which cameras have hard time with; a radar can see through fog where visible light cannot penetrate. Sensor fusion helps a car or any sensing robot to understand and track its environment which could consist of other cars, barriers on the shoulder of the road, motorcycles, bicycles, or pedestrians walking across the road. In the previous project, we implemented sensor fusion using extended Kalman filter (EKF) to combine lidar and radar installed on the car to track a bicycle in front of the car. Unscented Kalman filter (UKF) is an alternative technique to deal with nonlinear process and/or measurement models. Instead of linearlizing a nonlinear function as in EKF, the UKF uses something called sigma points to approximate the probability distributions. Sigma points model nonlinear transition better than linearization, and there is no need to calculate the Jacobian matrix.

The goal of this project is to implement the sensor fusion algorithm using UKF to combine lidar and radar installed on the car to track a bicycle in front of the car.

##  Process model 

In the EKF project, we used constant velocity (CV) model, which is too simplistic because it assumes that vehicles will travel in a straight line, whereas most roads have turns, in which case such a model will predict turning vehicles incorrectly. For example, for a vehicle trying to go in a circle, the CV model would assume the car is moving tangentially to the circle, resulting in a predicted position outside of it. In this project based on UKF, we use constant turn rate and velocity magnitude (CTRV) model, for which the state vector is x = [$p_{x}$, $p_{y}$, $v$, $ψ$, $ψ˙$], where $p_{x}$ and $p_{y}$ are $x$ and $y$ co-ordinates of the position, $v$ is the velocity, $ψ$ is the turning angle called yaw, and $ψ˙$ is the yaw rate. A car driving in a straight path has a $ψ˙$ of 0. For the same $ψ˙$, a car with slower speed has a smaller turning radius.

Figure 1 depicts the CTRV process model. The car is at position ($p_{x}$, $p_{y}$) at time $k$. The goal of the process model $f$ is to predict where the car will be at time $k+1$: $x_{k+1}$ = $f(x_{k}, ν_{k})$, where $ν$ is the process noise. The process noise is described by [$ν_{a,k}$, $ν_{ψ¨,k}$], where $ν_{a,k}$ is longitudinal acceleration noise that influences the longtitudinal speed of the vehicle, and $ν_{ψ¨,k}$ is the yaw acceleration noise that influences the yaw rate of the vehicle.

[image1]: ./figures/CTRV_process_model.jpg "CTRV process model"
![alt text][image1]
**Figure 1 CTRV process model**

Figure 2 depicts the CTRV process model equations.

[image2]: ./figures/CTRV_model_equations.jpg "CTRV process model equations"
![alt text][image2]
**Figure 2 CTRV process model equations**

##  UKF  model 

The way UKF processes measurements over time is exactly the same as the EKF, consisting of prediction and update steps. We have a prediction step which is independent of the measurement model. In the prediction step, we use the CTRV process model equations. In the measurement update step, we use lidar measurement model or radar measurement model depending on which sensor the measurement is coming from. The difference is in how UKF deals with nonlinear process and measurement models.

Predicting with a nonlinear function provides a distribution which is generally not normally distributed anymore and hence can be calculated only numerically. Though the predicted distribution is not normally distributed, UKF keeps going as if the predicted distribution is still normally distributed. What it does is find normal distribution that approximates the real distribution very closely - with the same mean and covariance matrix as the real distribution. How does it find these parameters?

###  Sigma points 

Since it can be difficult to transform the whole state distribution through a nonlinear function, individual representative points of the state space called sigma points are chosen around the mean state and in a certain relation to the standard deviation of every state dimension. These sigma points are inserted into the nonlinear prediction function, and the mean and covariance are calculated from the predicted sigma points. This, in many cases, gives useful approximation to the mean and covariance of the real predicted distribution. If the process model (prediction function) is linear, the sigma points provide the exact same solution as the standard Kalman filter.

###  UKF implementation details 

Prediction involves three steps. First, we need to know a good way to choose sigma points. Second, we need to know how to predict sigma points using the nonlinear process model. Third, we need to calculate the predicted mean and covariance matrix from the predicted sigma points. 

####  Choosing sigma points 

At the beginning of the prediction step, we have the posterior state $x_{k/k}$ and posterior covariance matrix $P_{k/k}$ from the last iteration, which together represent the distribution of the current state, for which we want to generate the sigma points. The number of sigma points depends on the state dimension, $n_{x}$ and noise dimension, $n_{n}$. The rule of thumb is 2*$n_{a}$+1, where $n_{a}$ = $n_{x}$+$n_{n}$ is the dimension of the augmented state vector. For CTRV model, $n_{x}$ = 5 and $n_{n}$ = 2, thus yielding 15 sigma points. First point is the mean of the state. Then, we have another two points per state dimension which represent spread in different directions. The rule for sigma point matrix is $X_{a,k/k}$ = [$x_{a/k}$     $x_{a/k}$ + $sqrt((λ + n_{a}).P_{a/k})$     $x_{a/k}$ - $sqrt((λ + n_{a}).P_{a/k})$], where $x_{a/k}$ is the first column, $x_{a/k}$ + $sqrt((λ + n_{a}).P_{a/k}$) is second through $n_{a}$+1 columns, and $x_{a/k}$ - $sqrt((λ + n_{a}).P_{k/k})$ is $n_{a}$+2 column through 2.$n_{a}$+1 column.

λ is a design parameter which helps with choosing the sigma points in relation to the error ellipse quantified by $P_{k/k}$. Larger the λ, further the sigma points move from the mean state; on the other hand, smaller the λ, closer the sigma points move to the mean state. Usually, λ = 3 - $n_{a}$ is a good design choice. 

With sigma point generation, we know how to represent the uncertainty of the posterior state estimation with sigma points. The uncertainty caused by nonlinear process noise are handled by the additional four sigma points contributed by the $n_{n}$ component above. 

As depicted in Figure 3, the state vector $x_{k/k}$ with the noise components is 7-dimensional and is called augmented state vector, $x_{a,k/k}$. The process covariance matrix $P_{k/k}$ (of size 5x5) with the noise covariance matrix Q added is of size 7x7 and is called augmented covariance matrix, $P_{a/k}$. Thus, sigma point matrix $X_{a,k/k}$ is of size 7x15.

[image3]: ./figures/UKF_augmentation_equations.jpg "UKF augmentation equations"
![alt text][image3]
**Figure 3 UKF augmentation equations**

####  Sigma point prediction 

For the prediction step depicted in Figure 4, we insert every sigma point (column of sigma point matrix $X_{a,k/k}$) 
into the process model, i.e., nonlinear process function $f(x_{k}, ν_{k})$. The output predicted sigma point matrix 
$X_{k+1/k}$ is of size 5x15 consisting of 15 5-dimensional column vectors representing predicted state.

[image4]: ./figures/Sigma_point_prediction.jpg "Sigma point prediction"
![alt text][image4]
**Figure 4 Sigma point prediction**

####  Mean and covariance of the predicted state

We use predicted sigma points in $X_{k+1/k}$ to calculate the mean $x_{k+1/k}$ and covariance $P_{k+1/k}$ of the predicted state as depicted in Figure 5 using the weights $w_{i}$, where $i$ is the specific column index. Earlier, we had state vector and covariance matrix, using which we generated sigma points. Now, we are doing the reverse step of recovering the mean state and covariance matrix from the predicted sigma points. Essentially, we have to invert the spreading of the sigma points - this is what the weights $w_{i}$ do.

[image5]: ./figures/Predicted_mean_and_standard_deviation.jpg "Predicted mean and standard deviation"
![alt text][image5]
**Figure 5 Predicted mean and standard deviation**


####  Predicted measurement: *Moving to measurement space*

We are done with the prediction step. Now, we can go into the measurement update step. As shown in Figure 6, first, we have to transform the predicted state into the measurement space using the measurement model depending on the sensor that produced the current measurement.

[image6]: ./figures/Predicted_measurement.jpg "Predicted measurement"
![alt text][image6]
**Figure 6 Predicted measurement**

Similar to the problem in the prediction step, we have to transform a distribution through a nonlinear measurement function. We can apply the same unscented transformation approach as we did before. However, we can take two short cuts and make it easier. Using the predicted mean and covariance matrix, first we generate the sigma points. However, a short cut is to reuse the sigma points we already have from the prediction step. Thus, we can skip the sigma points generation step. We can also skip the augmentation step, which was needed because the process noise had nonlinear effect on the state. In the measurement step for radar, the measurement model is a nonlinear function, but the measurement noise has purely additive effect. 

So, all we need to do is take the sigma points we already have, transform them into the measurement space as shown in Figure 6, and then use that to calculate the mean, $z_{k+1/k}$ and covariance matrix $S_{k+1/k}$ of the predicted measurement, as depicted in Figure 7.

[image7]: ./figures/Predicted_measurement_mean_and_standard_deviation.jpg "Predicted measurement mean and standard deviation"
![alt text][image7]
**Figure 7 Predicted measurement mean and standard deviation**

####  Measurement update 

This is the last step of the processing chain. We have the predicted state mean, $x_{k+1/k}$ and covariance matrix, $P_{k+1/k}$. We have the predicted measurement mean, $z_{k+1/k}$ and covariance matrix, $S_{k+1/k}$. We now receive the actual measurement $z_{k+1}$ for the first time. We can close the processing chain using the steps shown in Figure 8.

[image8]: ./figures/Measurement_update.jpg "Measurement update"
![alt text][image8]
**Figure 8 Measurement update**

###  Parameters and consistency 

Figure 9 depicts noise parameters present in the prediction and measurement update steps. 

[image9]: ./figures/Noise.jpg "Noise parameters"
![alt text][image9]
**Figure 9 Noise parameters**

In the process model used in the prediction step, we talked about the process noise $\nu_{k}$ consisting of $\sigma^2_{a}$ representing longitudinal acceleration noise (you might see this referred to as linear acceleration) and $\sigma^2_{\ddot\psi}$ representing yaw acceleration noise (this is also called angular acceleration)

In the project, both of these values will need to be tuned. A rule of thumb for getting useful values is the following. Try to estimate the maximum acceleration in your environment. Let us say, in urban environment, cars do not accelerate or brake stronger than $a_{max}$ = 6 $m/s^{2}$. Choose half of $a_{max}$ you expect as process noise. This implies $\sigma_{a}$ = 3 $m/s^{2}$ and $\sigma^2_{a}$ = 9 $m^{2}/s^{4}$. Whether this is a good value depends on your application. If it is important for your application to react fast on changes, then you choose the process noise a little higher. On the other hand, if it is important to provide smooth estimations, then choose the process noise a little lower.

Measurement noise parameters represent uncertainty in sensor measurements. In general, the manufacturer will provide these values in the sensor manual. Similar to the EKF project, in the UKF project also, we will not need to tune these parameters.

####  Consistency check 

To check if the noise parameters are set correctly, we can run the consistency check on the filter defined by the predicted measurement mean $z_{k+1/k}$ and covariance matrix $S_{k+1/k}$ calculated every time step. We then receive the actual measurement $z_{k+1}$ for that time step. Figure 10 depicts the concept of consistency filter. In the left are shown instances where the model constantly understimates the uncertainty of the predicted measurement (estimate less precise than we think). In the right are shown instances where the model constantly overstimates the uncertainty of the predicted measurement (estimate more precise than we think).

[image10]: ./figures/Inconsistency.jpg "Inconsistency"
![alt text][image10]
**Figure 10 Inconsistency**


As illustrated in Figure 11, to quantify the consistency of the filter, we use a metric called normalized innovation squared (NIS), which is the difference between the predicted and the actual measurement, and is called normalized because it is put in relation to the covariance matrix $S$. NIS is a scalar number and follows chi-squared distribution.

[image11]: ./figures/NIS.jpg "Normalized innovation squared (NIS)"
![alt text][image11]
**Figure 11 Normalized innovation squared (NIS)**

NIS does not tell you where the mistake comes from. If NIS << 7.815 for radar ($df$ = 3), reduce the process noise little bit; on the other hand, if NIS >> 7.815, increase the process noise little bit.



##  Performance check 

Performance check is done using root mean squared error (RMSE), which is the square root of the average squared difference  between the ground truth state vector $x_{truth}$ and estimated state vector *x*.

## Data

Here is a screenshot of the data file:

The simulator will be using this data file, and feed main.cpp values from it one line at a time.

[image12]: ./figures/Datafile.jpg "Screenshot of the data file"
![alt text][image12]
**Figure 12 Screenshot of the data file**

Each row represents a sensor measurement where the first column tells you if the measurement comes from radar (R) or lidar (L).

For a row containing radar data, the columns are: sensor_type, rho_measured, phi_measured, rhodot_measured, timestamp, x_groundtruth, y_groundtruth, vx_groundtruth, vy_groundtruth, yaw_groundtruth, yawrate_groundtruth.

For a row containing lidar data, the columns are: sensor_type, x_measured, y_measured, timestamp, x_groundtruth, y_groundtruth, vx_groundtruth, vy_groundtruth, yaw_groundtruth, yawrate_groundtruth.

Whereas radar has three measurements (rho, phi, rhodot), lidar has two measurements (x, y).


##  File structure 

The generalized overall flow consists of initializing UKF variables, predicting where the bicycle is going to be after a time step *Δt*, and updating where our object is based on sensor measurements. Then the prediction and update steps repeat themselves in a loop. To measure how well our UKF performs, we will then calculate root mean squared error comparing the Kalman filter results with the provided ground truth. These three steps (initialize, predict, update) plus calculating RMSE encapsulate the entire UKF project.

Following are the project files in the *src* folder:

* *main.cpp* - 
    - communicates with the Term 2 Simulator receiving data measurements
    - calls a function to run the UKF in *ukf.cpp*
    - calls a function to calculate RMSE in *tools.cpp*. 
* *ukf.cpp* - 
    - initializes the filter
    - calls the predict function
    - calls the update function. 
* *tools.cpp* - calculates RMSE and cartesian-to-polar conversion.

The Term 2 simulator is a client, and the C++ program software is a web server. *main.cpp* reads in the sensor data line by line from the client and stores the data into a measurement object that it passes to the UKF for processing. Also a ground truth list and an estimation list are used for tracking RMSE. *main.cpp* is made up of several functions within main(), these all handle the uWebsocketIO communication between the simulator and it's self.

Within *main()*,  the code creates an instance of *UKF* class, receives the measurement data by calling the *ProcessMeasurement()*, which is responsible for the initialization of the UKF as well as calling the prediction and update steps of the UKF. Finally, the *main()* will output estimated position and RMSE for each state component to the simulator.

RMSE is calculated for $p_{x}$, $p_{y}$, $v_{x}$, $v_{y}$, $\psi$, and $\dot\psi$.

##  Results 

Experiments were carried out for the following:
* for different values of initialization for state transition matrix P and position and velocity components of state *x*; positions $p_{x}$ and $p_{y}$ and velocity $v_{y}$ were initialized to 0.
* process noise - $\sigma_{a}$ and $\sigma_{\ddot\psi}$
* using only radar measurements
* using only lidar measurements
* combined radar and lidar measurements. 

| Sensor   | P      | $v_{x}$ | $\sigma_{a}$| $\sigma_{\ddot\psi}$ | $rmse_{px}$| $rmse_{py}$| $rmse_{vx}$ | $rmse_{vy}$ |$rmse_{ψ}$|$rmse_{\dot\psi}$|
|:-------: |:----:  |:-------:|:------:|:--------:|:----------:|:----------:|:-----------:|:-----------:|:--------:|:----------|
| Combined | 100    |5.0      | 2      | 2        | 0.09       | 0.31       | 0.47        | 0.76        | 0.16     | 1.05     |
| Combined | 1      |5.0      | 2      | 2        | 0.07       | 0.09       | 0.29        | 0.56        | 0.12     | 0.37     |
| Combined | 0.1    |5.0      | 2      | 2        | 0.07       | 0.09       | 0.20        | 0.25        | 0.06     | 0.13     |
| Combined | 0.0225 |0.0      | 2      | 2        | 0.11       | 0.09       | 0.48        | 0.22        | 0.05     | 0.12     |
| Combined | 0.0225 |0.0      | 0.01   | 0.01     | 1.08       | 1.19       | 1.32        | 1.03        | 0.34     | 0.24     |
| Combined | 0.0225 |5.0      | 30     | 30       | 0.09       | 0.12       | 0.76        | 0.96        | 0.18     | 0.91     |
| Combined | 0.0225 |5.0      | 2      | 2        | 0.07       | 0.09       | 0.19        | 0.23        | 0.05     | 0.12     |
| Lidar    | 0.0225 |5.0      | 2      | 2        | 0.10       | 0.10       | 0.27        | 0.28        | 0.07     | 0.13     |
| Radar    | 0.0225 |5.0      | 2      | 2        | 0.16       | 0.24       | 0.22        | 0.30        | 0.06     | 0.13     |

The stipulated RMSE requirements (<= [.09, .10, .40, .30]) were achieved under the following conditions: 
* $v_{x}$ was initialized to 5.0 for dataset 1 and -5.0 to dataset 2
* $\sigma_{a}$ <= 2, $\sigma_{\ddot\psi}$ <= 2
* both radar and lidar sensors were used. 

The initial values of P did not seem to have much effect. Radar was much noisier than lidar. In addition, angle normalization was extremely critical. The best RMSE values achieved were 0.07, 0.09, 0.19, and 0.23 for $p_{x}$, $p_{y}$, $v_{x}$, and $v_{y}$, respectively. These are better compared to EKF, for which the best values were 0.09, 0.08, 0.30, and 0.39, respectively.

Figures 13 and 14 depict the tracking performance of the UKF algorithm for dataset 1 and dataset 2.

[image13]: ./figures/Dataset1_UKF.jpg "Dataset 1: Tracking performance of UKF algorithm"
![alt text][image13]
**Figure 13 Dataset 1: Tracking performance of UKF algorithm**

[image14]: ./figures/Dataset2_UKF.jpg "Dataset 2: Tracking performance of UKF algorithm"
![alt text][image14]
**Figure 14 Dataset 2: Tracking performance of UKF algorithm**

Figure 15 compares the tracking performance of the UKF and EKF algorithms with respect to the ground truth for $p_{x}$, $p_{y}$, $v_{x}$, $v_{y}$. As one can see, UKF tracks ground truth quite well compared to EKF, especially for velocity components. Thus, the CTRV model follows turn quite well and provides a smooth position estimate. 

[image15]: ./figures/UKF_vs_EKF.jpg "Tracking performance of UKF Vs EKF algorithms"
![alt text][image15]
**Figure 15 Tracking performance of UKF Vs EKF algorithms**

None of the sensors is able to directly observe the orientation, but as shown in Figure 16, UKF still provides a precise estimate of yaw angle. Even the yaw rate can be estimated providing useful results. For AVs, the yaw rate of other vehicles is very important to know. Imagine another car starting to change lanes or a bicycle in front wants to take a left turn. Hopefully, they would both signalize their intention, but in the end, the estimated yaw rate is the ultimate indicator for such a behavior pattern.

[image16]: ./figures/Yaw_params.jpg "Orientation and yaw rate"
![alt text][image16]
**Figure 16 Orientation and yaw rate**

Figure 17 illustrates the superior performance of sensor fusion on tracking performance as compared to lidar only and radar only based tracking. As one can see, radar-only case provides the most noisy tracking, followed by lidar-only case, followed by EKF-based sensor fusion. UKF-based sensor fusion yields the best performance.

[image17]: ./figures/Sensor_fusion.jpg "Impact of sensor fusion"
![alt text][image17]
**Figure 17 Impact of sensor fusion**

Finally, Figure 18 depicts the importance of NIS metric in quantifying consistency. With $\sigma_{a}$ = 0.01, $\sigma_{\ddot\psi}$ = 0.01, uncertainty is highly underestimated; with $\sigma_{a}$ = 30, $\sigma_{\ddot\psi}$ = 30, uncertainty is slightly overestimated. $\sigma_{a}$ = 2, $\sigma_{\ddot\psi}$ = 2 gives best overall performance.

[image18]: ./figures/NIS_performance.jpg "Quantifying consistency"
![alt text][image18]
**Figure 18 Quantifying consistency**

<video controls src="videos/UKF_demo.mp4" />

## ** Important properties of UKF as a sensor fusion tool ** ##

* We can take noisy measurement data as input and provide a smooth position and velocity estimate of dynamic objects around us, without introducing a delay.

* We can estimate orientation and yaw rate of all the vehicles using sensors that cannot even directly observe these      things.

* We also get information on how precise the result is because UKF always provides a covariance matrix for every estimation -  we can check consistency to see if the covariance matrix is realistic. The uncertainty of the estimation results is very important for AVs because if the position of the leading vehicle is quite uncertain at some time, we better keep a little more distance.


### **Acknowledgments**

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio.

