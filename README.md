## Bicycle tracking using the sensor fusion algorithm based on unscented kalman filter

A self-driving car expert takes input from all the data sources and turns them into coherent picture. For example, a lidar is able to see distances which cameras have hard time with; a radar can see through fog where visible light cannot penetrate. Sensor fusion helps a car or any sensing robot to understand and track its environment which could consist of other cars, barriers on the shoulder of the road, motorcycles, bicycles, or pedestrians walking across the road. In the previous project, we implemented sensor fusion using extended kalman filter (EKF) to combine lidar and radar installed on the car to track a bicycle in front of the car. Unscented kalman filter (UKF) is an alternative technique to deal with nonlinear process and/or measurement models. Instead of linearlizing a nonlinear function as in EKF, the UKF uses something called sigma points to approximate the probability distributions. Sigma points model nonlinear transition better than linearization, and there is no need to calculate the Jacobian matrix.

The goal of this project is to implement the sensor fusion algorithm using UKF to combine lidar and radar installed on the car to track a bicycle in front of the car.

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Unscented-Kalman-Filter-Project).

## Code base

The project was implemented in Visual Studio in Windows 10.

1. In the CarND-Unscented-Kalman-Filter-Project, open the ukf.sln file.
2. Rebuild Solution.
3. Open the simulator.
4. Click F5 in Visual Studio to debug. This attaches the code to the simulator.
5. Select "Dataset 1" and click "Start" on the simulator.

To compile the code in Linux or Mac OS or Docker environment, 
1. Go to CarND-Unscented-Kalman-Filter-Project\src folder
1. Delete main.cpp 
2. Rename main_other_env.cpp as main.cpp.

## Detailed writeup

Detailed report can be found in [_UKF_writeup.md_](UKF_writeup.md).

## Solution video

![](./videos/UKF_demo.mp4)

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio.

